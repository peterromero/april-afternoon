<?php
/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
switch ($view->name) {
	case 'music_catalog': {
		// foreach ($fields as $id => $field) {
		// 	if (!empty($field->separator)) {
		// 		echo($field->separator);
		// 	}
		// 	switch($field->class) {
		// 		case 'title': {
		// 			$block_wrapper_intro = "<div class='teaser'><div class='plus_button'></div><div class='teaser_content'>";
		// 			$block_wrapper_outro = "";
		// 			break;
		// 		}
		// 		case 'field-credits': {
		// 			$block_wrapper_intro = "";
		// 			$block_wrapper_outro = "</div></div>";
		// 			break;
		// 		}
		// 		case 'field-ensemble-1': {
		// 			$block_wrapper_intro = "<div class='content_rollout'>";
		// 			$block_wrapper_outro = "";
		// 			break;
		// 		}
		// 		case 'add-to-cart-form': {
		// 			$block_wrapper_intro = "";
		// 			$block_wrapper_outro = "</div>";
		// 			break;
		// 		}
		// 		default: {
		// 			$block_wrapper_intro = "";
		// 			$block_wrapper_outro = "";
		// 			break;
		// 		}
		// 	}
		// 	echo($block_wrapper_intro);
		// 		echo($field->wrapper_prefix);
		// 			echo($field->label_html);
		// 			echo($field->content);
		// 		echo($field->wrapper_suffix);
		// 	echo($block_wrapper_outro);
		// }
		break;
	}
	default: {
		foreach ($fields as $id => $field) {
			if (!empty($field->separator)) {
				echo($field->separator);
			}
			echo($field->wrapper_prefix);
				echo($field->label_html);
				echo($field->content);
			echo($field->wrapper_suffix);
		}
		break;
	}
}
