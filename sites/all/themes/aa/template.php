<?php
function aa_colorbox_imagefield($variables) {
  $class = array('colorbox');
  if ($variables['image']['style_name'] == 'hide') {
    $image = '';
    $class[] = 'js-hide';
  }
  elseif ($variables['image']['style_name'] == 'loupe') {
    $image = '';
    $class[] = 'loupe';
    // $variables['title'] = 'Preview Page';
  }
  elseif (!empty($variables['image']['style_name'])) {
    $image = theme('image_style', $variables['image']);
  }
  else {
    $image = theme('image', $variables['image']);
  }
  $options = array(
    'html' => TRUE,
    'attributes' => array(
      'title' => $variables['title'],
      'class' => implode(' ', $class),
      'rel' => $variables['gid'],
    ),
  );
  return l($image, $variables['path'], $options);
}

function aa_preprocess_field(&$variables) {
  $key = array_search('clearfix', $variables['classes_array']);
  if ($key !== FALSE) {
    unset($variables['classes_array'][$key]);
  }
}

function aa_preprocess_page(&$vars) {
  if (_aa_is_portfolio_page()) {
    $vars['theme_hook_suggestions'][] = 'page__portfolio';
  }
  if (_aa_is_contact_form()) {
    $vars['theme_hook_suggestions'][] = 'page__contact';
  }
}

function aa_preprocess_html(&$vars) {
  if (_aa_is_portfolio_page()) {
    $vars['classes_array'][] = 'portfolio-page';
  }
  if (_aa_is_contact_form()) {
    $vars['classes_array'][] = 'contact-form';
  }
}

function aa_preprocess_region(&$vars) {
  if ($vars['region'] == 'header') {
    $vars['classes_array'][] = 'block_container';
  }
}

function aa_preprocess_menu_block_wrapper(&$vars) {
  if ($vars['config']['menu_name'] == 'menu-aa-nav' && _aa_is_portfolio_page()) {
    // Reverse order of links in AA Nav menu on portfolio pages.
    $links = element_children($vars['content']);
    $reversed_links = array_reverse($links);
    $swap = array();
    foreach ($links as $key) {
      $swap[$key] = $vars['content'][$key];
      unset($vars['content'][$key]);
    }
    foreach ($reversed_links as $key) {
      $vars['content'][$key] = $swap[$key];
    }
  }
}

function _aa_is_portfolio_page() {
  $portfolio_page_root_arguments = array(
    'design',
    'web',
    'art',
    'video',
    'portfolio'
  );
  $args = explode('/', request_path());
  if (in_array($args[0], $portfolio_page_root_arguments) && !isset($args[1])) {
    return TRUE;
  }
  return FALSE;
}

function _aa_is_contact_form() {
  $contact_form_paths = array(
    'node/16',
    'node/18',
    'node/19',
    'node/133',
    'node/138'
  );
  if (in_array(current_path(), $contact_form_paths)) {
    return TRUE;
  }
  return FALSE;
}

function __aa_is_taxonomy_term_page() {
  $args = explode('/', current_path());
  if (count($args) >= 2) {
    if ($args[0] == 'taxonomy' && $args[1] == 'term') {
      return TRUE;
    }
  }
  return FALSE;
}

function aa_preprocess_views_view_field(&$vars) {
  if ($vars['field']->field == 'field_video') {
    // We have to wrap the content one layer deeper so that CSS can make the
    // content image full height.
    $vars['output'] = '<span class="video_wrapper">' . $vars['output'] . '</span>';
  }
}
