<?php

/**
 * Implements hook_node_view().
 */
function aa_misc_node_view($node, $view_mode, $langcode) {
  if ($node->type == 'course_product_wrapper') {
    if ($view_mode == 'teaser') {
      // Only show the first course thumbnail in teaser view.
      $thumbnails = element_children($node->content['field_thumbnail']);
      $num_thumbnails = count($thumbnails);
      for ($i = 1; $i < $num_thumbnails; $i++) {
        unset($node->content['field_thumbnail'][$i]);
      }

      // Wrap everything but the thumbnail in a div so we can treat it all as one
      // grid block.
      $node->content['body']['#prefix'] = '<div class="product-details-wrapper">';
      $node->content['field_product']['#suffix'] = '</div>';
    }

    // Change text on disabled products to "Coming soon!"
    if (!empty($node->content['field_product'][0]['submit']['#attributes']['disabled'])) {
      global $base_url;
      $node->content['field_product'][0]['submit']['#value'] = 'Coming soon!';
      $node->content['field_product'][0]['submit']['#attributes']['title'] = "We're working hard to put the finishing touches on this!";
      $node->content['notify'] = [
        '#type' => 'markup',
        '#markup' => '<p><a href="' . $base_url . '/courses/notify">Sign up</a> to be notified when this content is available.</p>',
        '#prefix' => '<div class="disabled-product-notification">',
        '#suffix' => '</div></div>',
        '#weight' => 10,
      ];
      unset($node->content['field_product']['#suffix']);
    }

    if (!empty($node->content['field_product']['#object'])) {
      global $user;

      // Check if the user has access to the course's paid content.
      if ($product_id = $node->content['field_product']['#object']->field_product['und'][0]['product_id']) {
        if ($course_paid_content = node_load($course_paid_content_nid = _aa_misc_get_paid_content_nid_from_course_wrapper($product_id))) {
          if ($current_user_has_access_to_course_paid_content = node_access('view', $course_paid_content, $user)) {
            // If so, remove the "Add to Cart" button.
            unset($node->content['field_product']);

            // Add a link to the course content instead.
            global $base_url;
            $node->content['notify'] = [
              '#type' => 'markup',
              '#markup' => '<p><a href="' . $base_url . '/' . drupal_get_path_alias('node/' . $course_paid_content_nid) . '">You have purchased this course. View the content.</a></p>',
              '#prefix' => '<div class="course-access-notification">',
              '#suffix' => '</div></div>',
              '#weight' => 10,
            ];
          }
        }
      }
    }
  }
}

function _aa_misc_get_paid_content_nid_from_course_wrapper($cwid) {
  $query = db_select('field_data_field_paid_content', 'pc');
  $query->fields('pc', ['field_paid_content_target_id']);
  $query->condition('pc.entity_id', $cwid);
  $result = array_keys($query->execute()
                             ->fetchAllAssoc('field_paid_content_target_id'));
  return $result[0];
}

/**
 * Implements hook_form_alter().
 */
function aa_misc_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id === 'views_form_music_add_to_cart_form_block') {
    $form_state['cache'] = false;
  }
}