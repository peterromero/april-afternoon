function snap_footer_to_bottom() {
  if (jQuery('footer.site').length) {
    var window_height = jQuery(window).height() * 1;
    var footer_height = jQuery('footer.site').height() * 1;
    var body_height = jQuery('body').height() * 1;
    var footer_margin_top = jQuery('footer.site').css('margin-top');
    footer_margin_top = footer_margin_top.substr(0, footer_margin_top.length - 2) * 1;
    var footer_padding_top = jQuery('footer.site').css('padding-top');
    footer_padding_top = footer_padding_top.substr(0, footer_padding_top.length - 2) * 1;
    var footer_padding_bottom = jQuery('footer.site').css('padding-bottom');
    footer_padding_bottom = footer_padding_bottom.substr(0, footer_padding_bottom.length - 2) * 1;
    var footer_position = jQuery('footer.site').css('position');
    // console.log('window_height: ' + window_height + '; footer_height: ' + footer_height + '; body_height: ' + body_height + '; footer_margin_top: ' + footer_margin_top + '; footer_position: ' + footer_position + '; footer_padding_top: ' + footer_padding_top + '; footer_padding_bottom: ' + footer_padding_bottom);
    switch (footer_position) {
      case 'relative': {
        if (body_height < window_height) {
          var top = window_height - footer_height - footer_margin_top - footer_padding_top - footer_padding_bottom;
          jQuery('footer.site').css('position', 'absolute').css('top', top);
        }
        break;
      }
      case 'absolute': {
        if (body_height + footer_height + footer_margin_top + footer_padding_top + footer_padding_bottom >= window_height) {
          var top = 0;
          jQuery('footer.site').css('position', 'relative').css('top', top);
        } else {
          var top = window_height - footer_height - footer_margin_top - footer_padding_top - footer_padding_bottom;
          jQuery('footer.site').css('top', top);
        }
        break;
      }
    }
  }
}

function readout_width() {
  var jsw = jQuery(window).width();
  var cssw = jsw + 17;
  var h = jQuery(window).height();
  // console.log('Width (Actual): ' + jsw + 'px; Width (CSS): ' + cssw + "px; Height: " + h + 'px');
}

(function ($) {
  $(function () {
    $('.hamburger_menu_opener').click(function() {
      $('.header_menu').slideToggle(300);
    });
    imagesLoaded('.width_limiter.main', snap_footer_to_bottom);
    readout_width();
    $(window).resize(function () {
      snap_footer_to_bottom();
      readout_width();
    });
    $('.tooltip').click(function () {
      $(this).fadeOut(500);
      $.post(
        '/sites/all/themes/aa/ajax/tooltip_hide.php',
        {'id': $(this).attr('data-id')}
      );
    });
  });
})(jQuery);
