<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<?php
	require_once(dirname(__FILE__) . "/functions.inc");
	$num_licensed_files = get_user_licensed_file_count($user->uid);
?>
<header class="site">
	<div class="width_limiter">
		<div class="block_container logo">
			<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
				<img src="/sites/all/themes/aa/images/logo_white_text_only.png" alt="<?php print t('Home'); ?>" title="April Afternoon" width="150px" height="21px" />
			</a>
		</div>
		<nav class="site block_container">
			<?php $arg1 = arg(0); ?>
			<ul>
				<li><a href="/music" class="col1<?php if($arg1 == 'music') {echo(' active');} ?>">Music</a></li>
				<li><a href="/web" class="col2<?php if($arg1 == 'web') {echo(' active');} ?>">Web</a></li>
				<li><a href="/design" class="col3<?php if($arg1 == 'design') {echo(' active');} ?>">Design</a></li>
				<li><a href="/art" class="col4<?php if($arg1 == 'art') {echo(' active');} ?>">Art</a></li>
			</ul>
		</nav>
	</div>
</header>
<div class="width_limiter">
	<!--
	-->
</div>
<div class="width_limiter main">
	<div class="block_container">
		<div class="content_without_sidebar">
			<h1><?php echo($title); ?></h1>
			<?php echo($messages); ?>
			<?php if ($tabs) {echo("<div>" . render($tabs) . "</div>");} ?>
			<?php if ($action_links) {echo("<ul>" . render($action_links) . "</ul>");} ?>
			<?php echo(render($page['content'])); ?>
		</div>
	</div>
</div>
<footer class="site">
	<div class="width_limiter">
		<div class="block_container">
			<div class="block logo">
				<a href="/" title="Home">&nbsp;</a>
			</div>
			<?php print render($page['footer']); ?>
		</div>
		<div class="block_container">
			<p class="copyright">Website and contents copyright ©<?php echo(date('Y')); ?> by April Afternoon, all rights reserved.</p>
		</div>
	</div>
</footer>