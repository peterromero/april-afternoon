var currentTracks = {};
(function ($) {
  $(function () {
    // Grab the raw video tags before VideoJS modifies them (we just need their
    // IDs).
    var $players = $('.video-js');

    // Set a function to be called after each player is ready to go.
    $players.each(function () {
      videojs(this.id).ready(function () {
        currentTracks[this.id()] = 0;
        this.on('ended', function () {
          clearCurrentTrackHighlight(this.id());
        });
        this.on('pause', function () {
          setTrackHighlights(this.id(), currentTracks[this.id()], 'paused');
        });
        this.on('play', function () {
          setTrackHighlights(this.id(), currentTracks[this.id()], 'playing');
        });
      });
    });

    // Allow playlist to trigger playback/pause on its associated player.
    var $playlistLinks = $('.playlist a');

    $playlistLinks.click(function (e) {
      e.preventDefault();
      var playerId = $(this).data('player-id');
      var $player = videojs(playerId);

      if ($(this).hasClass('playing')) {
        $player.pause();
      } else {
        if ($(this).hasClass('paused')) {
          $player.play();
        } else {
          currentTracks[playerId] = $(this).data('track-number');
          var videoUrl = this.rel;
          $player.src({
            src: videoUrl,
            type: 'application/x-mpegURL'
          });
          $player.play();
        }
      }
    });

    function setTrackHighlights(playerId, highlightedTrackIndex, className) {
      clearCurrentTrackHighlight(playerId);
      var $trackLink = $('.playlist-' + playerId + ' a.track-' + highlightedTrackIndex);
      $trackLink.addClass(className);
      if (className == 'playing') {
        $trackLink.addClass('has-had-play-clicked');
      }
    }

    function clearCurrentTrackHighlight(playerId) {
      $('.playlist-' + playerId + ' a').removeClass('playing').removeClass('paused');
    }
  });
})(jQuery);

