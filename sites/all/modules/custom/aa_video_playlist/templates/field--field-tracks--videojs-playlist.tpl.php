<div class="<?php print $classes ?>" <?php print $attributes; ?>>
  <?php
  if (count($items)) {
    $first_track = $items[0];
    $posters = field_get_items('node', $element['#object'], 'field_poster');
    $poster_url = file_create_url($posters[0]['uri']);
    $id = uniqid('aavjs_');
    echo <<<HTML
<video id="$id" class="video-js vjs-16-9 vjs-big-play-centered" controls preload="auto" poster="$poster_url" data-setup="{}">
  <source src="{$first_track['#element']['display_url']}" type='application/x-mpegURL'>
</video>
HTML;
    echo("<ul class='playlist playlist-$id'>");
    foreach ($items as $key => $item) {
      echo("<li><a href='#' rel='{$item['#element']['display_url']}' data-player-id='$id' class='track-$key' data-track-number='$key'>{$item['#element']['title']}</a></li>");
    }
    echo('</ul>');
  }
  ?>
</div>
