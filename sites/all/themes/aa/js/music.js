var loupe_clicked = false;
var info_clicked = false;
function replace_checkboxes_with_styled_spans() {
	var $checkbox;
	var i = 0;
	jQuery('.form-item-field-instrumentation-tid .form-type-bef-checkbox label').each(function() {
		if (jQuery(this).text().substr(0,1) != '-') {
			i++;
			jQuery(this).parents('.form-type-bef-checkbox').addClass('section_header').attr('data-rollout-set', i);
		} else {
			jQuery(this).parents('.form-type-bef-checkbox').addClass('section_item').attr('data-rollout-set', i);
			jQuery(this).text(jQuery(this).text().substr(1));
		}
	});
	jQuery('#views-exposed-form-music-catalog-page input[type=checkbox]').each(function() {
		$checkbox = jQuery(this);
		$checkbox.addClass('hidden');
		if (!$checkbox.parent().hasClass('section_header')) {
			$checkbox.parent().prepend('<span class="checkbox ' + $checkbox.attr('id') + (($checkbox.attr('checked'))?' checked':'') + '"></span>');
			$checkbox.siblings('label').hover(function() {
				jQuery(this).siblings('.' + jQuery(this).attr('for')).addClass('hover');
			}, function() {
				jQuery(this).siblings('.' + jQuery(this).attr('for')).removeClass('hover');
			}).click(function() {
				jQuery(this).siblings('span.checkbox').toggleClass('checked');
			});
		}
	});
	jQuery('span.checkbox').click(function() {
		jQuery(this).toggleClass('checked');
		if (jQuery(this).hasClass('checked')) {
			jQuery(this).siblings('input[type=checkbox]').attr('checked', 'checked');
		} else {
			jQuery(this).siblings('input[type=checkbox]').attr('checked', '');
		}
		jQuery(this).parents('form').submit();
	});
	jQuery('.form-type-bef-checkbox.section_header').each(function() {
		jQuery(this).siblings('.section_item[data-rollout-set="' + jQuery(this).attr('data-rollout-set') + '"]').wrapAll('<div style="display: none;" class="rollout_menu" data-rollout-set="' + jQuery(this).attr('data-rollout-set') + '">');
	}).click(function(e) {
		jQuery('.rollout_menu[data-rollout-set="' + jQuery(this).attr('data-rollout-set') + '"]').slideToggle(200);
		jQuery(this).toggleClass('open');
		e.preventDefault();
	});
	// jQuery('.rollout_menu .checked').parents('.rollout_menu').css('display', 'block');
	jQuery('.rollout_menu .checked').each(function() {
		var $self = jQuery(this);
		var $menu = $self.parents('.rollout_menu');
		$menu.css('display', 'block');
		$self.parents('.bef-checkboxes').find('.section_header[data-rollout-set="' + $menu.attr('data-rollout-set') + '"]').addClass('open');
	});
	jQuery('#block-views-exp-music-catalog-page').fadeIn(500);
}
(function($) {
	$(function() {
		replace_checkboxes_with_styled_spans();
		$('audio').bind('ended', function() {
			$(this).siblings('.play_button').removeClass('playing');
		});
		$('.sunday #edit-checkout, .sunday #edit-continue').click(function(e) {
			alert('April Afternoon is closed on Sundays in U.S. Mountain Time (UTC-7:00). Please visit us again tomorrow!');
			e.preventDefault();
			return false;
		});
		$('.view-music-catalog .views-row .teaser').click(function(e) {
			if (loupe_clicked) {
				loupe_clicked = false; //Prevent clicks on the loupe from expanding/collapsing the row.
			} else if (info_clicked) {
				info_clicked = false; //Prevent clicks on the info button from expanding/collapsing the row.
			} else {
				$(this).parents('.views-row').toggleClass('expanded').find('.content_rollout').slideToggle(200);
			}
		});
		$('.form-text').focus(function() {
			$(this).select();
		});
		$('.play_button, .play_button_big').click(function(e) {
			var $button = $(this);
			var player = $(this).siblings('audio').get(0)
			if (player.paused) {
				player.play();
				$button.addClass('playing');
			} else {
				player.pause();
				$button.removeClass('playing');
			}
			return false;
		});
		$('.info_button').click(function (e) {
			info_clicked = true;
		});
		$('.colorbox.loupe').click(function(e) {
			loupe_clicked = true;
		});
		$('.field-name-field-add-to-cart .form-submit').click(function(e) { //Validate form to make sure there is a non-zero quantity in at least one product box.
			var copies_were_ordered = false;
			var single_copy_ordered = false;
			var $first_field = null;
			var num_copies_for_product;
			$(this).parents('form').find('.form-text').each(function() {
				if ($(this).attr('id').indexOf('add-to-cart-quantity')) {
					if (!$first_field) {
						$first_field = $(this);
					}
					num_copies_for_product = $(this).val();
					if (num_copies_for_product > 0) {
						copies_were_ordered = true;
					}
					if (num_copies_for_product == 1) {
						single_copy_ordered = true;
					}
				}
			});
			if (!copies_were_ordered) {
				alert('Please specify the number of copies you would like to order.');
				$first_field.focus();
				e.preventDefault();
			}
			if (single_copy_ordered) {
				var one_copy_ok = confirm("We noticed that you ordered 1 copy of a piece. If you would like to print multiple copies, i.e. for choral use, please click \"Cancel\" and enter the number of copies you would like to print.\n\nIf you do not intend to print more than one copy, click OK.");
				if (!one_copy_ok) {
					$first_field.focus();
					e.preventDefault();
				}
			}
		});
	});
})(jQuery);
