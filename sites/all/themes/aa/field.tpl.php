<?php

/**
 * @file field.tpl.php
 * Default template implementation to display the value of a field.
 *
 * This file is not used and is here as a starting point for customization only.
 * @see theme_field()
 *
 * Available variables:
 * - $items: An array of field values. Use render() to output them.
 * - $label: The item label.
 * - $label_hidden: Whether the label display is set to 'hidden'.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - field: The current template type, i.e., "theming hook".
 *   - field-name-[field_name]: The current field name. For example, if the
 *     field name is "field_description" it would result in
 *     "field-name-field-description".
 *   - field-type-[field_type]: The current field type. For example, if the
 *     field type is "text" it would result in "field-type-text".
 *   - field-label-[label_display]: The current label position. For example, if
 *     the label position is "above" it would result in "field-label-above".
 *
 * Other variables:
 * - $element['#object']: The entity to which the field is attached.
 * - $element['#view_mode']: View mode, e.g. 'full', 'teaser'...
 * - $element['#field_name']: The field name.
 * - $element['#field_type']: The field type.
 * - $element['#field_language']: The field language.
 * - $element['#field_translatable']: Whether the field is translatable or not.
 * - $element['#label_display']: Position of label display, inline, above, or
 *   hidden.
 * - $field_name_css: The css-compatible field name.
 * - $field_type_css: The css-compatible field type.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_field()
 * @see theme_field()
 *
 * @ingroup themeable
 */

switch($element['#field_name']) {
	case 'field_credits': {
		$content_style = 'normal';
		$field_wrapper_intro = "";
		$field_wrapper_outro = "</div><!-- Teaser Content --></div><!-- Teaser -->";
		break;
	}
	case 'field_ensembles': {
		$content_style = 'normal';
		$field_wrapper_intro = "<div class='content_rollout'>";
		$field_wrapper_outro = "";
		break;
	}
	case 'field_add_to_cart': {
		$content_style = 'normal';
		$field_wrapper_intro = "";
		$field_wrapper_outro = "</div>";
		break;
	}
	case 'field_preview_audio': {
		$content_style = 'field_preview_audio';
		break;
	}
	case 'field_is_one_of_multiple_arrange': {
		$content_style = 'field_is_one_of_multiple_arrange';
		break;
	}
	default: {
		$content_style = 'normal';
		$field_wrapper_intro = "";
		$field_wrapper_outro = "";
		break;
	}
}

switch($content_style) {
	case 'field_preview_audio': {
		// $audio_url = $items[0];
		?>
		<div class="<?php echo($classes); ?>"<?php echo($attributes); ?>>
			<div class="field-item">
				<a class="play_button" title="Listen to a Preview"></a>
				<audio>
					<?php
						foreach($items as $item) {
							$audio_url = render($item);
							$extension = substr($audio_url, -3);
							echo("<source src=\"$audio_url\" type=\"audio/$extension\">");
						}
					?>
					Sorry; your browser doesn't support this audio preview.
				</audio>
			</div>
		</div>
		<?php
		break;
	}
	case 'field_is_one_of_multiple_arrange': {
		$field_data = render($items[0]);
		if ($field_data == 'multi') {
			?>
			<div class="<?php echo($classes); ?>"<?php echo($attributes); ?>>There is more than one arrangement of this piece. Make sure you're looking at the correct one by checking the ensemble(s) above.</div>
			<?php
		} else {
			?>
			<div></div>
			<?php
		}
		break;
	}
	default: {
		?>
		<?php echo($field_wrapper_intro); ?>
		<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
		  <?php if (!$label_hidden): ?>
		    <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>:&nbsp;</div>
		  <?php endif; ?>
		  <div class="field-items"<?php print $content_attributes; ?>>
		    <?php foreach ($items as $delta => $item): ?>
		      <div class="field-item <?php print $delta % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$delta]; ?>><?php print render($item); ?></div>
		    <?php endforeach; ?>
		  </div>
		</div>
		<?php echo($field_wrapper_outro); ?>
		<?php
	}
}
?>
