<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 *
 * @ingroup themeable
 */
$is_sunday = false;
$dow = date('w', time());
if ($dow == '0') {
	$is_sunday = true;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo($head); ?>
	<title><?php echo($head_title); ?></title>
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
	<meta name="viewport" content="minimum-scale=1.0, maximum-scale=1.0, width=device-width, user-scalable=no">
	<link rel="apple-touch-icon-precomposed" href="/sites/all/themes/aa/images/touch-icons/57.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/sites/all/themes/aa/images/touch-icons/72.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/sites/all/themes/aa/images/touch-icons/114.png">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/sites/all/themes/aa/images/touch-icons/144.png">

	<meta name="DC.title" content="April Afternoon" />
	<meta name="geo.region" content="US-CO" />
	<meta name="geo.placename" content="Greeley" />
	<meta name="geo.position" content="40.422964;-104.690462" />
	<meta name="ICBM" content="40.422964, -104.690462" />
	<meta name="keywords" content="graphic design, adobe creative suite, illustrator, logos, branding, drupal">
	<meta name="description" content="Graphic design for print and screen, branding, typography, web, and music">
	<meta property="og:title" content="Design and Sound for Screen, Video, and Print">
	<meta property="og:description" content="Graphic design for print and screen, branding, typography, web, and music">
	<meta property="og:type" content="website">
	<meta property="og:url" content="http://aprilafternoon.com/">
	<meta property="og:site_name" content="April Afternoon">
	<meta property="og:image" content="http://aprilafternoon.com/sites/all/themes/aa/images/og_image.jpg">

	<?php print $styles; ?>
	<?php print $scripts; ?>
	<script type="text/javascript" src="//use.typekit.net/ulb1urr.js"></script>
	<script type="text/javascript">
		try{
			Typekit.load({
				active: function() {
					console.log('Typekit loaded');
				}
			});
		} catch(e) {}
	</script>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-57021960-1', 'auto');
		ga('send', 'pageview');
	</script>
</head>
<body class="<?php echo($classes); if ($is_sunday) {echo(' sunday');} ?>" <?php echo($attributes); ?>>
	<div id="skip-link">
		<a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
	</div>
	<?php print $page_top; ?>
	<?php print $page; ?>
	<?php print $page_bottom; ?>
</body>
</html>
