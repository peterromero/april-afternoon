<?php
	function get_user_licensed_file_count($uid) {
		$q = "
			SELECT
				count(file_managed_field_data_commerce_file_license_file.fid) AS num
			FROM
			{commerce_file_license} commerce_file_license
			LEFT JOIN {field_data_commerce_file_license_file} field_data_commerce_file_license_file ON commerce_file_license.license_id = field_data_commerce_file_license_file.entity_id AND (field_data_commerce_file_license_file.entity_type = 'commerce_file_license' AND field_data_commerce_file_license_file.deleted = '0')
			INNER JOIN {file_managed} file_managed_field_data_commerce_file_license_file ON field_data_commerce_file_license_file.commerce_file_license_file_fid = file_managed_field_data_commerce_file_license_file.fid
			WHERE (( (commerce_file_license.uid = '$uid' ) ))
		";
		$files = db_query($q)->fetchObject();
		return $files->num;
	}
