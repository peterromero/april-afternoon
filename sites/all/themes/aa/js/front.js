(function ($) {
  $(function () {
    // Set up click destinations for front page service blocks.
    $('.region-triptych .block').click(function () {
      switch (this.id) {
        case 'block-block-2':
          window.location.href = 'design';
          break;
        case 'block-block-3':
          window.location.href = 'music/catalog';
          break;
        case 'block-block-4':
          window.location.href = 'video';
          break;
      }
    });
  });
})(jQuery);
