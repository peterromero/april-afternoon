<header class="site">
	<div class="width_limiter">
		<div class="block_container logo">
			<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
				<img src="/sites/all/themes/aa/images/logo_white.png" alt="<?php print t('Home'); ?>" title="April Afternoon" width="150px" height="45px" />
			</a>
		</div>
	</div>
</header>
<div class="width_limiter main">
	<div class="block_container">
		<div class="content_without_sidebar">
			<h1><?php echo($title); ?></h1>
			<?php echo($messages); ?>
			<?php echo(render($page['content'])); ?>
		</div>
	</div>
</div>
